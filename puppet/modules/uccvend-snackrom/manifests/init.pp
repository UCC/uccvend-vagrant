# == Class: snackrom
#
class uccvend-snackrom {
	package { 'binutils-m68hc1x':
	  ensure => present,
	}

	package { 'gcc-m68hc1x':
	  ensure => present,
	}

	package { 'lzip':
	  ensure => present,
	}

	vcsrepo { "/vagrant/code/uccvend-snackrom":
	  ensure => present,
	  provider => git,
	  require => [ Package[ 'git' ] ],
	  source => "git://git.ucc.asn.au/uccvend-snackrom.git",
	  revision => 'master',
	}
}
