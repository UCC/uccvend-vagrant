# == Class: virtualsnack
#
class virtualsnack {
	vcsrepo { "/vagrant/code/virtualsnack":
	  ensure => present,
	  provider => git,
	  require => [ Package[ 'git' ] ],
	  source => "https://github.com/ucc/virtualsnack.git",
	  revision => 'master',
	}
}
