# == Class: opendispense2
#
class opendispense2 {
	package { 'libncurses5-dev':
	  ensure => present,
	}
	
	package { 'libsqlite3-0':
	  ensure => present,
	}
	
	package { 'libsqlite3-dev':
	  ensure => present,
	}
	
	package { 'libident':
	  ensure => present,
	}
	
	package { 'libident-dev':
	  ensure => present,
	}
	
	package { 'libmodbus5':
	  ensure => present,
	}
	
	package { 'libmodbus-dev':
	  ensure => present,
	}

	# Needed for dispense server to function correctly	
	package { 'pidentd':
	  ensure => present,
	}
	
	vcsrepo { "/vagrant/code/opendispense2":
	  ensure => present,
	  provider => git,
	  require => [ Package[ 'git' ] ],
	  source => "https://github.com/ucc/OpenDispense2.git",
	  revision => 'master',
	}


	file { '/etc/opendispense2':
	  ensure => 'directory',
	}


        file { '/etc/opendispense2/dispsrv.conf':
          ensure => file,
          content => template('opendispense2/dispsrv.conf.erb'),
        }

        file { '/etc/opendispense2/items.cfg':
          ensure => file,
          content => template('opendispense2/items.cfg.erb'),
        }
}
