# == Class: virtualcoke
#
class virtualcoke {
	package { 'python-pymodbus' :
	  ensure => present
	}

	package { 'python-twisted' :
	  ensure => present
	}


	vcsrepo { "/vagrant/code/virtualcoke":
	  ensure => present,
	  provider => git,
	  require => [ Package[ 'git' ] ],
	  source => "https://github.com/ucc/virtualcoke.git",
	  revision => 'master',
	}
}
