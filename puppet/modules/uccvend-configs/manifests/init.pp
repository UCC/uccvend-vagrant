class uccvend-configs {
	# Directory for dispense config
	file { "/etc/opendispense":
		ensure => directory;
	}

	# Client config file for dispense
	file { 'dispense-client.conf':
		path	=>	'/etc/opendispense/client.conf',
		ensure	=>	file,
		content	=>	template("uccvend-configs/dispense-client.conf.erb"),
	}
	
	# Server config file for dispense
	file { 'dispense-server.conf':
		path	=>	'/etc/opendispense/dispsrv.conf',
		ensure	=>	file,
		content	=>	template("uccvend-configs/dispense-server.conf.erb"),
	}

	# Items config file for dispense

	file { 'dispense-items.cfg':
		path	=>	'/etc/opendispense/items.cfg',
		ensure	=>	file,
		content	=>	template("uccvend-configs/dispense-items.cfg.erb"),
	}

}
