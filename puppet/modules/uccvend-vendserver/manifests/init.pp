# == Class: vendserver
#
class uccvend-vendserver {
	package { 'python-ldap':
	  ensure => present,
	}

	vcsrepo { "/vagrant/code/uccvend-vendserver":
	  ensure => present,
	  provider => git,
	  require => [ Package[ 'git' ] ],
	  source => "git://git.ucc.asn.au/uccvend-vendserver.git",
	  revision => 'master',
	}

	file { '/etc/dispense2':
	  ensure => 'directory',
	}

	file { '/etc/dispense2/servers.conf':
	  ensure => file,
	  content => template('uccvend-vendserver/servers.conf.erb'),
	}
}
