exec { 'apt-get update':
  path => '/usr/bin',
}

package { 'vim':
  ensure => present,
}

package { 'git':
  ensure => present,
}

file { "/vagrant/code":
	ensure => directory,
}

# common dependencies

package { 'python3':
	ensure => present,
}

package { 'python-pip':
          ensure => present,
}

package { 'npyscreen':
          provider => pip,
          ensure => present,
}



include uccvend-snackrom
include virtualsnack
include virtualcoke
include opendispense2
include uccvend-vendserver

# setup configs for development environment
include uccvend-configs
