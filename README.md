# UCCVend Vagrant


## Description

A Vagrantfile and set of puppet manifests/modules to set up the UCC vending
suite of software in a Virtualbox VM for development.

## Usage

(first time)

Make sure you Vagrant install is at a current version

$ vagrant box add ubuntu/trusty32

(normal)

$ vagrant up

## Notes

Repositories are checked out anonymously.   To be able to submit patches
back use:
	git format-patch

## Author

Mark Tearle <mark@tearle.com>
