#!/bin/bash

# RunTestEnvironment.sh

# This script will run a test environment inside tmux

# error handling
abort()
{
    echo >&2 '*** ABORTED ***'
    echo "An error occurred. Exiting..." >&2
    exit 1
}

trap 'abort' 0

set -e

# If an error occurs, the abort() function will be called.
#----------------------------------------------------------

# Done!
trap : 0

# Start new tmux session

tmux new -d -s uccvend

tmux new-window -t uccvend:1 -n 'dispense' 'watch date'
tmux new-window -t uccvend:2 -n 'Vendserver' 'watch date'
tmux new-window -t uccvend:3 -n 'virtualsnack' 'watch date'
tmux new-window -t uccvend:4 -n 'virtualcoke' 'watch date'

tmux new-window -t uccvend:5 -n 'Shell' 
 
 
tmux select-window -t uccvend:5
tmux -2 attach-session -t uccvend

