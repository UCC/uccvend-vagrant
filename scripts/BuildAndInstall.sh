#!/bin/bash

# BuildAndInstall.sh

# This script will build the various bits of the UCC Vend infrastructure
# and run their install scripts

# error handling
abort()
{
    echo >&2 '*** ABORTED ***'
    echo "An error occurred. Exiting..." >&2
    exit 1
}

trap 'abort' 0

set -e

# If an error occurs, the abort() function will be called.
#----------------------------------------------------------


CODEDIR=/vagrant/code

# build and install dispense

cd $CODEDIR/opendispense2/src

make
sudo make install

# build SNACKROM

cd $CODEDIR/uccvend-snackrom/ROM2

make

# vendserver 

cd $CODEDIR/uccvend-vendserver

python setup.py build
sudo python setup.py install

# virtualsnack
# virtualcoke

# for the moment, until these are changed to use setuptools, left
# in /vagrant/code

# Done!
trap : 0

