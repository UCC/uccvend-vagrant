#!/bin/bash

# InitialConfig.sh

# This script will set up some initial users with some 
# credit and add them to various groups to allow them to do various things

# error handling
abort()
{
    echo >&2 '*** ABORTED ***'
    echo "An error occurred. Exiting..." >&2
    exit 1
}

trap 'abort' 0

set -e

# If an error occurs, the abort() function will be called.
#----------------------------------------------------------

DISPENSE=/usr/local/opendispense2/dispense

# Set up groups

sudo addgroup --gid 10021 gumby

# let vagrant user do useful stuff

sudo $DISPENSE user add vagrant
sudo $DISPENSE user type vagrant admin 
sudo $DISPENSE user type vagrant coke 
sudo $DISPENSE user type vagrant door 

# Setup some users

# alice		- run of the mill UCC user
# bob		- wheel member
# chuck		- committee member
# dave		- expired user
# eve		- second run of mill UCC user
# murphy	- wheel and coke/door member

#alice
sudo adduser --gecos "Alice" --ingroup gumby alice
sudo $DISPENSE user add alice

#bob
sudo adduser --gecos "Bob" --ingroup root bob
sudo $DISPENSE user add bob

#chuck
sudo adduser --gecos "Chuck" --ingroup gumby chuck
sudo $DISPENSE user add chuck
sudo $DISPENSE user type chuck admin

#dave
sudo adduser --gecos "Dave" --ingroup gumby dave --disabled-password --disabled-login
sudo $DISPENSE user add dave
sudo $DISPENSE user type dave disabled

#eve
sudo adduser --gecos "Eve" --ingroup gumby eve
sudo $DISPENSE user add eve

#murphy
sudo adduser --gecos "ACC Murphy" --ingroup root murphy
sudo $DISPENSE user add murphy
sudo $DISPENSE user type murphy admin 
sudo $DISPENSE user type murphy coke 
sudo $DISPENSE user type murphy door 


# Set some balances
$DISPENSE acct alice +1000 "Initial"
$DISPENSE acct bob -4567 "Poor bastard"
$DISPENSE acct chuck +1500 "Initial"
$DISPENSE acct dave +2500 "Initial"
# eve has no balance
$DISPENSE acct murphy +1975 "Initial"

# Done!
trap : 0

